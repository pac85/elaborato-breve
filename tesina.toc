\contentsline {section}{\numberline {1}Teoria delle categorie}{3}{}%
\contentsline {subsection}{\numberline {1.1}Alcune categorie}{3}{}%
\contentsline {subsection}{\numberline {1.2}Altri esempi di categoria}{3}{}%
\contentsline {section}{\numberline {2}Relazione tra teoria delle categorie ed insiemistica}{4}{}%
\contentsline {subsection}{\numberline {2.1}La categoria \textbf {Set}}{4}{}%
\contentsline {section}{\numberline {3}Costruzioni universali}{5}{}%
\contentsline {subsection}{\numberline {3.1}Isomorfismi}{5}{}%
\contentsline {section}{\numberline {4}Teoria delle categorie e programmazione}{6}{}%
\contentsline {subsection}{\numberline {4.1}Funzioni pure e non pure}{6}{}%
\contentsline {section}{\numberline {5}Modello matematico di un programma}{7}{}%
\contentsline {subsection}{\numberline {5.1}Polimorfismo}{7}{}%
\contentsline {subsection}{\numberline {5.2}Composizione in Haskell}{8}{}%
\contentsline {subsection}{\numberline {5.3}Funzioni in più argomenti}{9}{}%
\contentsline {section}{\numberline {6}Product}{9}{}%
\contentsline {subsection}{\numberline {6.1}Product in Haskell}{10}{}%
\contentsline {section}{\numberline {7}Funzioni in più variabili in haskell}{11}{}%
\contentsline {subsection}{\numberline {7.1}Con product}{11}{}%
\contentsline {subsection}{\numberline {7.2}Currying}{11}{}%
\contentsline {subsection}{\numberline {7.3}curry ed uncurry}{12}{}%
\contentsline {section}{\numberline {8}Coproduct}{13}{}%
\contentsline {subsection}{\numberline {8.1}Pattern matching}{14}{}%
\contentsline {subsection}{\numberline {8.2}Esempio di coproduct in Haskell}{15}{}%
\contentsline {section}{\numberline {9}Functors}{15}{}%
\contentsline {subsection}{\numberline {9.1}Functors in Haskell}{16}{}%
\contentsline {section}{\numberline {10}Typeclasses}{17}{}%
\contentsline {subsection}{\numberline {10.1}List in Haskell}{17}{}%
\contentsline {subsection}{\numberline {10.2}List come functor}{18}{}%
\contentsline {section}{\numberline {11}Functor composition}{18}{}%
\contentsline {section}{\numberline {12}Product functor?}{20}{}%
\contentsline {section}{\numberline {13}Bifunctor}{20}{}%
\contentsline {section}{\numberline {14}Bibliografia}{22}{}%
